# mongoid-simple-tags 

Forked from https://github.com/hashdog/mongoid-simple-tags

Updated by tvarley for Ruby > 5.2

See: https://github.com/hashdog/mongoid-simple-tags/pull/23


mongoid-simple-tags is a basic and simple tagging system for mongoid using
map-reduce function (no backwards compatibility with mongoid v2!)

## Install

    Add the following to Gemfile:

      gem "mongoid-simple-tags"

## Usage

### Model

    class User
      include Mongoid::Document
      include Mongoid::Document::Taggable
    end

### Console

    u = User.create(name: "Tuquito", tag_list: "linux, tucuman, free software")

    u.tags     # => ["linux","tucuman","free software"]

    User.tagged_with("linux") # => [u]
    User.tagged_with(["tucuman", "free software"]) # => [u]
    User.tagged_with(["linux", "foo"]) # => [u]

    User.tagged_with_all(["linux"]) # => [u]
    User.tagged_with_all(["linux", "foo"]) # => []

    u2 = User.new(:name => "ubuntu")
    u2.tag_list = "linux"
    u2.save

    User.tagged_with("linux") # => [u, u2]
    User.tagged_with(["linux", "tucuman"]) # => [u, u2]

    User.tagged_with_all(["linux", "tucuman"]) # => [u]

    # using map-reduce function

    User.all_tags #=>[{:name=>"free software", :count=>1}, {:name=>"linux", :count=>2}, {:name=>"tucuman", :count=>1}]

    User.tag_list #=>["free software", "linux", "tucuman"]

## Give back

If you're using mongoig-simple-tags and you or your company is making money
from it, then please consider donating via
[Beerpay](https://beerpay.io/hashdog/mongoid-simple-tags) so that I can
continue to improve it.

## Contributors

*   lucas renan - https://github.com/lucasrenan


## Copyright

Copyright (c) 2011 chebyte(mauro torres). See LICENSE.txt for further details.
